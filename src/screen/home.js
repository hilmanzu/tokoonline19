import React, { Component } from "react";
import {Dimensions,StyleSheet,View,StatusBar,ScrollView,Image,TextInput,Text,TouchableOpacity,Alert,Modal,ActivityIndicator} from "react-native"
import styles from "../style/home"
import firebase from "react-native-firebase"
import store from 'react-native-simple-store';
import renderIf from '../assets/renderIf';
import Banner from '../component/banner'
import Topup from '../component/topup'
import Flash from '../component/flash'
import Geolocation from 'react-native-geolocation-service';

export default class home extends Component {

  static navigationOptions = {
  headerStyle: {
        position: 'absolute',
        top: 0,
        left: 0
      },
    tabBarLabel: 'Home',
    tabBarIcon: ({ tintColor }) => (
      <Image
        source={require('../assets/home.png')}
        style={[styles.tabicon, { tintColor: tintColor }]}
      />
    ),
  };

  constructor(props){
    super(props);
    this.state = {
      keranjang:''
    }  
  }

  componentDidMount() {
    Geolocation.getCurrentPosition(
      (position) => {
        store.save('latitude',position.coords.latitude)
        store.save('longitude',position.coords.longitude)
      },
      (error) => {
        console.log(error.code, error.message);
      },
      { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
    )

    store.get('uid').then((res)=>{
      const db = firebase.firestore()
      file = db.collection("user").doc(res).collection('keranjang')
      file.onSnapshot(async(querySnapshot)=>{
         var data = []
           querySnapshot.forEach((doc)=>{
             let item = data
             item.push({
               data : doc.data(),
               id   : doc.id
             })
           })
          this.setState({
           keranjang : data.length
         })
      })
    })
  }

  render() {
    const {datas} = this.state;

    return (
      <View style={styles.root}>
        <View style={styles.header}>
          <Text style={styles.headertext}>Order Online</Text>

          <TouchableOpacity style={{marginLeft:210}} onPress={(this.order)}>
            <Image source={require("../assets/keranjang.png")} style={{width:35,height:35,tintColor:'#fff'}}/>
            {renderIf(this.state.keranjang === 0 ? true : false)(
              <View/>
            )}
            {renderIf(this.state.keranjang !== 0 ? true : false)(
              <Text style={{marginBottom:20,position:'absolute',backgroundColor:'red',paddingLeft:8,paddingRight:8,paddingTop:5,paddingBottom:5,color:'#fff',borderRadius:100,fontSize:10}}>
                {this.state.keranjang}
              </Text>
            )}
          </TouchableOpacity>

        </View>
        <ScrollView contentContainerStyle={styles.scroll}>
          <View style={{position:'absolute',backgroundColor:'#293462',width:Dimensions.get('window').width,height:150}}>
          </View>
          <TouchableOpacity onPress={()=>this.props.navigation.navigate('sub_home')} style={{width:320,height:40,backgroundColor:'#fff',borderRadius:10,justifyContent:'center'}}>
            <Text style={{marginLeft:10,color:'#00000090'}}>Cari Barang</Text>
          </TouchableOpacity>
          <Banner/>
          <Text style={{fontSize:15,fontWeight:'bold'}}>Top Up & Tagihan</Text>
          <Topup/>
          <Flash/>
        </ScrollView>
      </View>
    );
  }
  order=()=>{
    this.props.navigation.navigate('order')
  }

}