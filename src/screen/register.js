import React, { Component } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Image,
  TextInput,
  Picker,
  Text,
  Switch,
  TouchableOpacity,
  Alert,
  Modal,
  ActivityIndicator
} from "react-native";
import { Center } from "@builderx/utils";
import styles from "../style/register"
import firebase from "react-native-firebase"
import store from 'react-native-simple-store';

export default class Registrasi extends Component {

  constructor(props){
    super(props);
    this.state = {
        email         :'',
        password      :'',
        ponsel        :'',
        nama          :'',
        loading       : false,
    }
  }

  render() {
    const { email,password,nama,ponsel } = this.state

    return (
      <View style={styles.root}>
        <ScrollView
          style={styles.scrollArea}
          contentContainerStyle={styles.scrollArea_contentContainerStyle}
        >
          <View horizontal>
            <Image
              source={require("../assets/logo.png")}
              resizeMode={"contain"}
              style={styles.image}
            />
          </View>
          <TextInput
            placeholder={"Nama Lengkap"}
            placeholderTextColor={"rgba(155,155,155,1)"}
            returnKeyType={"next"}
            onChangeText={(nama)=> this.setState({nama})}
            style={styles.nama}
          />
          <TextInput
            placeholder={"Ponsel"}
            placeholderTextColor={"rgba(155,155,155,1)"}
            keyboardType={"numeric"}
            returnKeyType={"next"}
            onChangeText={(ponsel)=> this.setState({ponsel})}
            style={styles.email}
          />
          <TextInput
            placeholder={"Email"}
            placeholderTextColor={"rgba(155,155,155,1)"}
            keyboardType={"email-address"}
            returnKeyType={"next"}
            onChangeText={(email)=> this.setState({email})}
            style={styles.email}
          />
          <View>
            <TextInput
              placeholder={"Password"}
              placeholderTextColor={"rgba(155,155,155,1)"}
              keyboardType={"password"}
              returnKeyType={"next"}
              secureTextEntry={true}
              onChangeText={(password)=> this.setState({password})}
              style={styles.password}
            />
          </View>
          <TouchableOpacity style={styles.buttondaftar} onPress={this.register}>
            <Text style={styles.text5}>DAFTAR</Text>
          </TouchableOpacity>
          <View horizontal>
            <Text style={styles.footertext}>
              Online Order{"\n"}© 2019 All right reserved. TOKOKARYA
            </Text>
          </View>
        </ScrollView>
        <Modal
          animationType="fade"
          transparent={true}
          visible={this.state.loading}>
          <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <View style={{width:150,height:100,backgroundColor:'#fff',alignItems:'center',justifyContent:'center',elevation:5,borderRadius:10}}>
              <Text style={{marginBottom:5}}>Sedang memproses</Text>
              <ActivityIndicator size="large" color="#0000ff" />
            </View>
          </View>
        </Modal>
      </View>
    );
  }

  register=()=>{
    const { email,password,nama,ponsel } = this.state

    var text = this.state.password

    if (this.state.nama == ''){
      alert('Isi Nama Lengkap')
    }else if (this.state.ponsel == ''){
      alert('Isi ponsel anda')
    }else if (this.state.email == ''){
      alert('Isi email anda')
    }else if ( text.length <= 7){
      alert('Password harus lebih 7 karakter')
    }else {
      this.setState({ loading: true });
      firebase.auth().createUserWithEmailAndPassword(email, password)
        .then((response) => {
          if (response.user.uid){
            this.setState({ loading: false });
            const db = firebase.firestore()
            db.collection('user').doc(response.user.uid).set({
              nama           : nama,
              email          : email,
              ponsel         : ponsel,
              role           : 'user',
              id_user        : response.user.uid,
              image          : 'https://www.fote.org.uk/wp-content/uploads/2017/03/profile-icon-300x300.png'
            })
            .then(()=>{
              store.save('uid', response.user.uid)
              store.save('role','user')
              store.save('nama',nama)
              Alert.alert('Hallo','Selamat anda telah terdaftar')
              this.props.navigation.navigate('menu');
            })
            .catch((error)=>{
              this.setState({ loading: false });
              alert(error)
            })
          }
        })

        .catch((error)=>{
          if ( error.code == 'auth/email-already-in-use' ){
          this.setState({ loading: false });
          Alert.alert('Akun Telah Digunakan','Silahkan daftar dengan email lain')
          }else if ( error.code == 'auth/user-not-found' ){
          this.setState({ loading: false });
          Alert.alert('Akun Tidak ditemukan','Silahkan daftar dengan email lain')
          }else if ( error.code == 'auth/wrong-password'){
          this.setState({ loading: false });
          Alert.alert('password anda salah',)
          }
        })
    }
  }

}