import React, {Component} from 'react';
import {Modal,BackHandler,Alert,Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from "../style/profile"
import store from 'react-native-simple-store';
import firebase from "react-native-firebase"

export default class menu extends Component{

  static navigationOptions = {
  headerStyle: {
        position: 'absolute',
        top: 0,
        left: 0
      },
    tabBarLabel: 'Profile',
    tabBarIcon: ({ tintColor }) => (
      <Image
        source={require('../assets/profile.png')}
        style={[{width:20,height:20}, { tintColor: tintColor }]}
      />
    ),
  };

  constructor(props) {
    super(props)
      this.state = {
        nama: '',
        image:'https://www.fote.org.uk/wp-content/uploads/2017/03/profile-icon-300x300.png',
        news:[],
        pengumuman:[]
      }
  }

  componentDidMount(){

    store.get('nama').then((res)=>{
        this.setState({ nama:res})
    })

    store.get('image').then((res)=>{
        this.setState({ image:res})
    })

  }

  render() {
  const { navigate } = this.props.navigation;

    return (
      <View style={styles.container}>
        <ScrollView contentContainerStyle={styles.scrolview}>
          <View style={styles.path1}>
            <Text style={styles.welcome}>Selamat Datang{"\n"}{this.state.nama}</Text>
          </View>
          <View style={styles.path2}>
            <Text style={{color:'#fff',marginLeft:10,marginTop:10,fontWeight:'bold',fontSize:15}}>Saldo :</Text>
            <Text style={{color:'#fff',marginLeft:10}}>Rp. 30,000,-</Text>
            <Text style={{color:'#fff',marginLeft:10,marginTop:10,fontWeight:'bold',fontSize:15}}>Credit Anda :</Text>
            <Text style={{color:'#fff',marginLeft:10}}>Rp. 30,000,-</Text>
          </View>
          <View style={styles.imageBack}>
              <TouchableOpacity onPress={(()=> this.props.navigation.navigate('profile') )}>
                <Image style={styles.image} source={{uri: this.state.image}}/>
              </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}