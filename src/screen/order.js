import React, {Component} from 'react';
import {Modal,TouchableHighlight,ActivityIndicator,Alert,Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../style/order'
import store from 'react-native-simple-store';
import moment from 'moment'
import 'moment/locale/id'
moment.locale('id')
import firebase from "react-native-firebase"
var numeral = require('numeral');

export default class checkin extends Component{
  constructor(props){
    super(props)
    this.delete = this.delete.bind(this);
    this.state = {
      dashboard:[],
      loading:false,
      touch:'',
      id_retail:'',
      retail:'',
      saldo :0,
      kredit:0,
      metode:'Pilih Metode'
      }
    }

  componentDidMount(){
    store.get('uid').then((res)=>{
      const db = firebase.firestore()
      file = db.collection("user").doc(res).collection('keranjang')
      profile = db.collection("user").doc(res)

      file.onSnapshot(async(querySnapshot)=>{
         var data = []
           querySnapshot.forEach((doc)=>{
             let item = data
             item.push({
               data : doc.data(),
               id   : doc.id
             })
           })
          this.setState({
           dashboard : data,
           id_retail : data[0].data.id_retail,
           retail    : data[0].data.retail
         })
      })

      profile.onSnapshot(async(doc)=>{
         let data = doc.data()
          this.setState({
           saldo : data.saldo,kredit:data.kredit
         })
      })

    })
  }

  render() {
  const { dashboard } = this.state
  let totalharga = dashboard.reduce((a,b)=>{
    a += parseInt(b.data.harga)
    return a
    }, 0)
  const harga = totalharga
  var total = 0

  if(harga === 0){
      return(
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headertext}>Order</Text>
        </View>
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
          <Image style={{width:50,height:50}}/>
          <Text>Keranjang masih kosong {"\n"} Ayo belanja sekarang</Text>
        </View>
      </View>
      )
    }

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headertext}>Order</Text>
        </View>
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow : 1, alignItems:'center'}}>
            {dashboard.map((data,index)=>
                <TouchableOpacity style={{flexDirection:'row',marginTop:20,width:Dimensions.get('window').width}} >
                  <Image style={{flex:1,width:85,height:85,marginHorizontal:25}} source={{uri:data.data.image}}/>
                  <View style={{flex:2}}>
                    <Text style={{width:140,color:'#000000',fontWeight:'bold',fontSize:18,marginTop:5}}>{data.data.nama}</Text>
                    <Text style={{color:'#000000',fontWeight:'bold',fontSize:14,color:'#ed5107'}}>Rp. {numeral(data.data.harga).format('0,0')}</Text>
                    <Text style={{color:'#000000',fontWeight:'bold',fontSize:15,marginTop:5}}>Item : {data.data.jumlah} {data.data.satuan}</Text>
                  </View>
                  <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                  <TouchableOpacity onPress={()=> this.delete(data)}>
                    <Image style={{width:30,height:30}} source={require('../assets/delete.png')}/>
                  </TouchableOpacity>
                  </View>
                </TouchableOpacity>
            )}

        </ScrollView>

        <View style={{justifyContent:'center',width:Dimensions.get('window').width,height:60,elevation:5,backgroundColor:'#fff',borderWidth:0.5}}>
          <View style={{marginLeft:20,marginRight:20}}>
            <Text style={{width:200,color:'#000000',fontWeight:'bold',fontSize:15}}>Total Tagihan</Text>
            <Text style={{color:'#00000090',fontSize:14,}}>Rp {numeral(harga).format('0,0')}</Text>
          </View>
        </View>

        <TouchableOpacity onPress={(()=> this.setState({loading:true}))} style={{justifyContent:'center',width:Dimensions.get('window').width,height:60,elevation:5,backgroundColor:'#fff',marginBottom:20}}>
          <View style={{marginLeft:20,marginRight:20}}>
            <Text style={{width:200,color:'#000000',fontWeight:'bold',fontSize:15}}>Metode Pembayaran</Text>
            <Text style={{color:'#00000090',fontSize:14,}}>{this.state.metode}</Text>
            <Image style={{width:20,height:20,position:'absolute',marginLeft:300,marginTop:10}} source={require('../assets/arrow.png')}/>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={this.absen} style={{width:180,height:50,borderRadius:10,marginBottom:10,elevation:1,justifyContent:'center',alignItems:'center',backgroundColor:'#293462'}}>
          <View>
            <Text style={{color:'#fff',fontWeight:'bold',fontSize:15}}>Order Sekarang</Text>
          </View>
        </TouchableOpacity>

        <Modal animationType="fade" transparent={true} visible={this.state.loading}>
          <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <View style={{backgroundColor:'#fff',width:300,height:300,elevation:10,borderRadius:10,alignItems:'center'}}>
              <TouchableOpacity onPress={(()=> this.setState({loading:false}))} style={{marginLeft:250,marginTop:5,alignItems:'center',justifyContent:'center',borderRadius:100}}>
                <Text style={{textAlign:'center'}}>tutup</Text>
              </TouchableOpacity>
              <Text style={{marginLeft:8,fontSize:18,fontWeight:'bold',color:'#000000',marginBottom:20,marginTop:20,width:300}} >Pilih metode pembayaran</Text>
              
              <TouchableOpacity onPress={this.saldo} style={{height:60,width:300}}>
                <Text style={{marginLeft:8,fontWeight:'bold'}}>Saldo</Text>
                <Text style={{marginBottom:20,marginLeft:8,fontSize:15,color:'#000000'}} >Saldo : Rp {numeral(this.state.saldo).format('0,0')}</Text>
                <Image style={{width:20,height:20,position:'absolute',marginLeft:265,marginTop:10}} source={require('../assets/check.png')}/>
              </TouchableOpacity>

              <TouchableOpacity onPress={this.kredit} style={{marginTop:2,height:60,width:300}}>
                <Text style={{marginLeft:8,fontWeight:'bold'}}>PayLater</Text>
                <Text style={{marginBottom:20,marginLeft:8,fontSize:15,color:'#000000'}} >Saldo : Rp {numeral(this.state.kredit).format('0,0')}</Text>
                <Image style={{width:20,height:20,position:'absolute',marginLeft:265,marginTop:10}} source={require('../assets/check.png')}/>
              </TouchableOpacity>

              <TouchableOpacity onPress={(()=> Alert.alert('Opss','Fitur ini segera hadir'))} style={{width:180,height:50,borderRadius:10,marginTop:20,elevation:1,justifyContent:'center',alignItems:'center',backgroundColor:'#293462'}}>
                <View>
                  <Text style={{color:'#fff',fontWeight:'bold',fontSize:15}}>Isi Saldo</Text>
                </View>
              </TouchableOpacity>

            </View>
          </View>
        </Modal>

      </View>
    );
  }

  absen =() => {
    const { dashboard } = this.state
    let totalharga = dashboard.reduce((a,b)=>{
      a += parseInt(b.data.harga)
      return a
      }, 0)
    const harga = totalharga
    var total = 0

    if (this.state.metode === 'Pilih Metode'){
      Alert.alert('Opss','Pilih metode pembayaran untuk melanjutkan transaksi')
    } else if (this.state.metode === 'saldo'){

      store.get('uid').then((uid)=>{
        store.get('nama').then((nama)=>{
          const db = firebase.firestore()
          db.collection("transaksi").doc().set({
            nama      : nama,
            retail    : this.state.retail,
            id_user   : uid,
            id_retail : this.state.id_retail,
            status    : 'menunggu',
            total     : harga,
            items     : dashboard,
            waktu     : new Date(),
            metode    : 'saldo'
          })
          .then(()=>{
            dashboard.forEach((doc)=>{
             db.collection('user').doc(uid).collection('keranjang').doc(doc.data.id).delete()
            })
            Alert.alert("Berhasil memilih barang","Lakukan pembayaran untuk melanjutkan transaksi")
            this.props.navigation.navigate('tagihan')
          })
        })
      })

    } else if (this.state.metode === 'kredit'){
      store.get('uid').then((uid)=>{
        store.get('nama').then((nama)=>{
          const db = firebase.firestore()
          db.collection("transaksi").doc().set({
            nama      : nama,
            retail    : this.state.retail,
            id_user   : uid,
            id_retail : this.state.id_retail,
            status    : 'menunggu',
            total     : harga,
            items     : dashboard,
            waktu     : new Date(),
            metode    : 'kredit'
          })
          .then(()=>{
            dashboard.forEach((doc)=>{
             db.collection('user').doc(uid).collection('keranjang').doc(doc.data.id).delete()
            })
            Alert.alert("Berhasil memilih barang","Lakukan pembayaran untuk melanjutkan transaksi")
            this.props.navigation.navigate('tagihan')
          })
        })
      })
    }

  }

  delete(data){
    const db = firebase.firestore()
    Alert.alert(
      'Opss',
      'Ingin menghapus data ini ?',
      [
        {
          text: 'tidak',
          onPress: () => console.log('Cancel Pressed'),
          style: 'tidak',
        },
        {text: 'OK', onPress: () => 
          store.get('uid').then((uid)=>{
            db.collection("user").doc(uid).collection('keranjang').doc(data.id).delete()
          })
        },
      ],
      {cancelable: false},
    );
  }

  saldo = () => {
    const { dashboard } = this.state
    let totalharga = dashboard.reduce((a,b)=>{
      a += parseInt(b.data.harga)
      return a
      }, 0)
    const harga = totalharga
    var total = 0

    if (this.state.saldo <= harga){
      Alert.alert('Oppss','Saldo anda tidak mencukupi silahkan isi ulang saldo')
    } else if (this.state.saldo >= harga){
      this.setState({metode:'saldo',loading:false})
    }
  }

  kredit = () => {
    const { dashboard } = this.state
    let totalharga = dashboard.reduce((a,b)=>{
      a += parseInt(b.data.harga)
      return a
      }, 0)
    const harga = totalharga
    var total = 0

    if (this.state.kredit <= harga){
      Alert.alert('Oppss','Limit kredit anda tidak mencukupi silahkan gunakan metoda lain')
    } else if (this.state.kredit >= harga){
      this.setState({metode:'kredit',loading:false})
    }
  }

}
