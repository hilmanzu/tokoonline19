import React, {Component} from 'react';
import {TouchableHighlight,ActivityIndicator,Alert,Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../style/order'
import store from 'react-native-simple-store';
import moment from 'moment'
import 'moment/locale/id'
moment.locale('id')
import firebase from "react-native-firebase"
var numeral = require('numeral');

export default class checkin extends Component{
  constructor(props){
    super(props)
    this.delete = this.delete.bind(this);
    this.state = {
      dashboard:[],
      loading:true,
      touch:'',
      id_retail:'',
      retail:''
      }
    }

  componentDidMount(){
    store.get('uid').then((res)=>{
      const db = firebase.firestore()
      file = db.collection("user").doc(res).collection('keranjang')
      file.onSnapshot(async(querySnapshot)=>{
         var data = []
           querySnapshot.forEach((doc)=>{
             let item = data
             item.push({
               data : doc.data(),
               id   : doc.id
             })
           })
          this.setState({
           dashboard : data,
           id_retail : data[0].data.id_retail,
           retail    : data[0].data.retail
         })
      })
    })
  }

  render() {
  const { dashboard } = this.state
  let totalharga = dashboard.reduce((a,b)=>{
    a += parseInt(b.data.harga)
    return a
    }, 0)
  const harga = totalharga
  var total = 0

  if(harga === 0){
      return(
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headertext}>Order</Text>
        </View>
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
          <Image style={{width:50,height:50}}/>
          <Text>Keranjang masih kosong {"\n"} Ayo belanja sekarang</Text>
        </View>
      </View>
      )
    }

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headertext}>Order</Text>
        </View>
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow : 1, alignItems:'center'}}>
            {dashboard.map((data,index)=>
                <TouchableOpacity style={{flexDirection:'row',marginTop:20,width:Dimensions.get('window').width}} >
                  <Image style={{flex:1,width:85,height:85,marginHorizontal:25}} source={{uri:data.data.image}}/>
                  <View style={{flex:2}}>
                    <Text style={{width:140,color:'#000000',fontWeight:'bold',fontSize:18,marginTop:5}}>{data.data.nama}</Text>
                    <Text style={{color:'#000000',fontWeight:'bold',fontSize:14,color:'#ed5107'}}>Rp. {numeral(data.data.harga).format('0,0')}</Text>
                    <Text style={{color:'#000000',fontWeight:'bold',fontSize:15,marginTop:5}}>Item : {data.data.jumlah} {data.data.satuan}</Text>
                  </View>
                  <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                  <TouchableOpacity onPress={()=> this.delete(data)}>
                    <Image style={{width:30,height:30}} source={require('../assets/delete.png')}/>
                  </TouchableOpacity>
                  </View>
                </TouchableOpacity>
            )}

        </ScrollView>

        <View style={{justifyContent:'center',width:Dimensions.get('window').width,height:60,elevation:5,backgroundColor:'#fff',borderWidth:0.5}}>
          <View style={{marginLeft:20,marginRight:20}}>
            <Text style={{width:200,color:'#000000',fontWeight:'bold',fontSize:15}}>Total Tagihan</Text>
            <Text style={{color:'#00000090',fontSize:14,}}>Rp {numeral(harga).format('0,0')}</Text>
          </View>
        </View>

        <TouchableOpacity onPress={()=> this.setState({loading:true})} style={{justifyContent:'center',width:Dimensions.get('window').width,height:60,elevation:5,backgroundColor:'#fff',marginBottom:20}}>
          <View style={{marginLeft:20,marginRight:20}}>
            <Text style={{width:200,color:'#000000',fontWeight:'bold',fontSize:15}}>Metode Pembayaran</Text>
            <Text style={{color:'#00000090',fontSize:14,}}>Saldo Kredit</Text>
            <Image style={{width:20,height:20,position:'absolute',marginLeft:300,marginTop:10}} source={require('../assets/arrow.png')}/>
          </View>
        </TouchableOpacity>

        <TouchableOpacity onPress={this.absen} style={{width:180,height:50,borderRadius:10,marginBottom:10,elevation:1,justifyContent:'center',alignItems:'center',backgroundColor:'#293462'}}>
          <View>
            <Text style={{color:'#fff',fontWeight:'bold',fontSize:15}}>Order Sekarang</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
