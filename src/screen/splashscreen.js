import React from 'react';
import {PermissionsAndroid,BackHandler,Alert,View, Text,  StyleSheet, Image ,Platform,ActivityIndicator} from 'react-native';
import store from 'react-native-simple-store';
import firebase from "react-native-firebase"

export default class Splashscreen extends React.Component {
    constructor(props){
        super(props)
          this.state = {
            data:'',
          }
      }

    componentDidMount(){
      try {
        PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            'title': 'Example App',
            'message': 'Example App access to your location '
          }
        )
      } catch (err) {
        console.warn(err)
      }


      store.get('uid').then((res)=>{
        if (res) {
          const db = firebase.firestore();
          const base = db.collection("user").doc(res)
          base.onSnapshot( async (doc) => {
            let data = doc.data()
            let id   = doc.id
            store.save('role',data.role)
            store.save('nama',data.nama)
            store.save('image',data.image)
            setTimeout(() => this.props.navigation.navigate('menu'), 2000)
          })
        } else {
          setTimeout(() => this.props.navigation.navigate('login'), 2000)
        }

      })

    }

    render() {
    return (
      <View style={styles.container}>
        <Image source={require("../assets/logo.png")} style={styles.image} />
        <Text style={{fontWeight:'bold',fontSize:20}}>Order Online</Text>
        <Text style={{marginTop:10,marginBottom:50}}>" Belanja Dimana Saja Jadi Lebih Mudah "</Text>
          <ActivityIndicator
            color={`rgba(0, 163, 192, 1)`}
            size={`large`}
            style={styles.spin}
          />
      </View>
        )
    }
}

const styles = StyleSheet.create ({
   container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  image: {
    width: 120,
    height: 80,
    marginBottom:20
  }
})