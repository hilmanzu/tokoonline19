import React, { Component } from "react";
import {ImageBackground,TouchableWithoutFeedback,Dimensions,StyleSheet,View,StatusBar,ScrollView,Image,TextInput,Text,TouchableOpacity,Alert,Modal,ActivityIndicator} from "react-native"
import styles from "../style/sub_home"
import firebase from "react-native-firebase"
import store from 'react-native-simple-store';
import renderIf from '../assets/renderIf';
import Banner from '../component/banner'
import Topup from '../component/topup'
import Flash from '../component/flash'
var numeral = require('numeral');

export default class home extends Component {

  static navigationOptions = {
  headerStyle: {
        position: 'absolute',
        top: 0,
        left: 0
      },
    tabBarLabel: 'Home',
    tabBarIcon: ({ tintColor }) => (
      <Image
        source={require('../assets/home.png')}
        style={[styles.tabicon, { tintColor: tintColor }]}
      />
    ),
  };

  constructor(props){
    super(props);
    this.state = {
      datas:'',
      list:[],
      loading:false,
      image   : '',
      nama    : '',
      harga   :0,
      id      : '',
      text:1,
      keranjang:'',
      id_retail:'',
      retail:''
    }  
  }

  componentDidMount(){

    store.get('uid').then((res)=>{
      const db = firebase.firestore()
      file = db.collection("user").doc(res).collection('keranjang')
      file.onSnapshot(async(querySnapshot)=>{
         var data = []
           querySnapshot.forEach((doc)=>{
             let item = data
             item.push({
               data : doc.data(),
               id   : doc.id
             })
           })
          this.setState({
           keranjang : data.length
         })
      })
    })

  }

  componentWillMount(){
    const db = firebase.firestore()
    file = db.collection("item barang")
    file.onSnapshot(async(querySnapshot)=>{
       var data = []
         querySnapshot.forEach((doc)=>{
           let item = data
           item.push({
             data : doc.data(),
             id   : doc.id
           })
         })
        this.setState({
         list : data
       })
     })
  }

  render() {
    const {datas,list} = this.state;
    return (
      <View style={styles.root}>
        <View style={styles.header}>
          <Text style={styles.headertext}>Order Online</Text>

          <TouchableOpacity style={{marginLeft:210}} onPress={(this.order)}>
            <Image source={require("../assets/keranjang.png")} style={{width:35,height:35,tintColor:'#fff'}}/>
            {renderIf(this.state.keranjang === 0 ? true : false)(
              <View/>
            )}
            {renderIf(this.state.keranjang !== 0 ? true : false)(
              <Text style={{marginBottom:20,position:'absolute',backgroundColor:'red',paddingLeft:8,paddingRight:8,paddingTop:5,paddingBottom:5,color:'#fff',borderRadius:100,fontSize:10}}>
                {this.state.keranjang}
              </Text>
            )}
          </TouchableOpacity>

        </View>
        <View style={{alignItems:'center',height:70}}>
          <TouchableOpacity style={{marginTop:20,marginBottom:20,width:320,height:40,backgroundColor:'#fff',borderRadius:10,justifyContent:'center',elevation:5}}>
            <TextInput 
              placeholder={"Cari"}
              placeholderTextColor={"rgba(155,155,155,1)"}
              returnKeyType={"next"}
              onChangeText={(filter)=> this.setState({filter})} 
              style={{marginLeft:20,color:'#00000090'}}/>
          </TouchableOpacity>
        </View>
        <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow:1,alignItems:'center'}}>
          <View style={{width:Dimensions.get('window').width,flexDirection: 'row',flexWrap: 'wrap',justifyContent: 'center'}}>
            {list.map((data)=>
              <TouchableOpacity onPress={(()=>this.list(data))} style={{width:150,height:195,marginTop:10,backgroundColor:'#FFFFFF',marginHorizontal:8,borderRadius:5,elevation: 10,justifyContent:'center'}}>
                <ImageBackground imageStyle={{borderRadius:5}} source={{uri : data.data.image}} style={{width:150,height:110,}}>
                </ImageBackground>
                <Text numberOfLines={2} style={{marginLeft:5,fontSize:16,fontWeight:'bold',marginTop:5}}>{data.data.nama}</Text>
                <Text numberOfLines={1} style={{marginLeft:5,fontSize:12,fontWeight:'bold',color:'#ed5107'}}>Rp. {numeral(data.data.harga).format('0,0')}</Text>
                <Text numberOfLines={1} style={{marginLeft:5}}><Image source={require('../assets/home.png')} style={{width:10,height:10,marginLeft:5,tintColor:'#420000',borderRadius:100}}/> {data.data.retail}</Text>
              </TouchableOpacity>
            )}
          </View>
        </ScrollView>
        <Modal animationType="fade" transparent={true} visible={this.state.loading}>
          <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <View style={{backgroundColor:'#fff',width:300,height:300,elevation:10,borderRadius:10}}>
              <TouchableOpacity onPress={this.cancel} style={{marginLeft:250,marginTop:5,alignItems:'center',justifyContent:'center',borderRadius:100}}>
                <Text style={{textAlign:'center'}}>tutup</Text>
              </TouchableOpacity>
              <Text style={{marginBottom:20,marginLeft:8,fontSize:18,fontWeight:'bold',color:'#000000'}} >{this.state.nama}</Text>
              
              <View style={{flex:1,alignItems:'center'}}>
                <Image source={{uri:this.state.image}} style={{flex:1,width:100,height:100}}/>
                <View style={{flex:1,justifyContent:'center',flexDirection:'row',marginTop:10}}>
                  <TouchableWithoutFeedback disabled={this.state.text !== 1 ? false : true} onPress={ () => this._decrementCount() }>
                   <Image style={{width:25,height:25}} source={require('../assets/minus.png')}/>
                  </TouchableWithoutFeedback>
                   <Text style={{paddingLeft:20,paddingRight:20,color:'#000000',fontSize:18}}> {this.state.text} </Text>
                  <TouchableWithoutFeedback onPress={ () => this._incrementCount() }>
                   <Image style={{width:25,height:25}} source={require('../assets/plus.png')}/>
                  </TouchableWithoutFeedback>
                </View>
                <View style={{flexDirection:'row',flex:1,marginLeft:10}}>
                  <View style={{flex:1}}>
                    <Text style={{fontSize:17,fontWeight:'bold',color:'#000000'}} >SubTotal</Text>
                    <Text style={{fontSize:15,fontWeight:'bold',color:'#ed5107'}} >{numeral(this.state.harga).format('0,0')}</Text>
                  </View>
                  <View style={{flex:1,alignItems:'flex-end',marginRight:10}}>
                    <TouchableOpacity style={{borderRadius:10,width:100,height:40,backgroundColor:'#293462',alignItems:'center',justifyContent:'center'}} onPress={this.cart}>
                      <Text style={{fontSize:15,color:'#fff',fontWeight:'bold'}}>Order</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>

            </View>
          </View>
        </Modal>
      </View>
    );
  }

  _incrementCount = () => {
     this.setState(prevState => ({ text: prevState.text + 1 , harga: this.state.harga_awal + this.state.harga }));
  }
  _decrementCount = () => {
     this.setState(prevState => ({ text: prevState.text - 1 , harga: this.state.harga - this.state.harga_awal }));
  }

  cart=()=>{
    store.get('uid').then((res)=>{
      const db = firebase.firestore()
      db.collection('user').doc(res).collection('keranjang').doc(this.state.id).set({
        image      : this.state.image,
        nama       : this.state.nama,
        harga      : this.state.harga,
        harga_awal : this.state.harga_awal,
        id         : this.state.id,
        jumlah     : this.state.text,
        satuan     : this.state.satuan,
        id_retail  : this.state.id_retail,
        retail     : this.state.retail
      })
      this.setState({loading:false,text:1})
      Alert.alert("Berhasil Menambah Barang","Konfirmasi pesanan anda di keranjang")
    })
  }

  list=(data)=>{
    this.setState({
      image         : data.data.image,
      nama          : data.data.nama,
      harga         : data.data.harga,
      id            : data.id,
      harga_awal    : data.data.harga,
      jumlah        : this.state.text,
      satuan        : data.data.satuan,
      id_retail     : data.data.id_retail,
      retail        : data.data.retail,
      loading       : true
    })
  }

  cancel=()=>{
    this.setState({loading:false,text:1})
  }

  order=()=>{
    this.props.navigation.navigate('order')
  }

}