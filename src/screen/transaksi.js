import * as React from 'react';
import { View, StyleSheet, Dimensions,Image,Text } from 'react-native';
import { TabView, SceneMap,TabBar } from 'react-native-tab-view';
import styles from '../style/tagihan'
//transaksi
import Pembelian from './pembelian'
import Tagihan from './tagihan'

const SecondRoute = () => (
  <Pembelian/>
);

const FirstRoute = () => (
  <Tagihan/>
);

export default class TabViewExample extends React.Component {

  static navigationOptions = {
  headerStyle: {
        position: 'absolute',
        top: 0,
        left: 0
      },
    tabBarLabel: 'Transaksi',
    tabBarIcon: ({ tintColor }) => (
      <Image
        source={require('../assets/transaksi.png')}
        style={[{width:20,height:20}, { tintColor: tintColor }]}
      />
    ),
  };

  state = {
    index: 0,
    routes: [
      { key: 'first', title: 'Tagihan' },
      { key: 'second', title: 'Pembelian' },
    ],
  };

  renderTabBar(props) {
    return (
      <TabBar
        style={{backgroundColor: '#293462', elevation: 0, borderColor: '#000000', borderBottomWidth: 1, height:50}}
        labelStyle={{color: '#fff', fontSize: 12, fontWeight: 'bold'}}
        {...props}
        indicatorStyle={{backgroundColor: '#00818A', height: 3}}
      />
    );
  }

  render() {
    return (
      <TabView
        navigationState={this.state}
        renderScene={SceneMap({
          first: FirstRoute,
          second: SecondRoute,
        })}
        onIndexChange={index => this.setState({ index })}
        initialLayout={{ width: Dimensions.get('window').width }}
        renderTabBar={this.renderTabBar}
        renderHeader={this.renderHeader}
      />
    );
  }
}