import React, {Component} from 'react';
import {TouchableWithoutFeedback,Modal,TouchableHighlight,ActivityIndicator,Alert,Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../style/tagihan'
import store from 'react-native-simple-store';
import moment from 'moment'
import 'moment/locale/id'
moment.locale('id')
import firebase from "react-native-firebase"
import renderIf from '../assets/renderIf'
var numeral = require('numeral');

export default class tagihan extends Component{

  constructor(props){
    super(props)
    this.delete = this.delete.bind(this);
    this.state = {
      data:'',
      loading:true,
      touch:'',
      id:'',
      view:false
      }
    }

  componentDidMount(){
    const {params} = this.props.navigation.state;
    store.get('uid')
    .then((res)=>{
      const db = firebase.firestore();
      const base = db.collection("transaksi").doc(params.id)
      base.onSnapshot((doc)=>{
        let data = doc.data()
        let id   = doc.id
        this.setState({
            data : data,loading:false,id : id
          })
        })
    })

  }

  render() {
  const { data,id,loading } = this.state
  const items = data.items

    if (loading === true){
      return(
        <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
          <View style={{width:150,height:100,backgroundColor:'#fff',alignItems:'center',justifyContent:'center',elevation:5,borderRadius:10}}>
            <Text style={{marginBottom:5}}>Sedang memproses</Text>
            <ActivityIndicator size="large" color="#0000ff" />
          </View>
        </View>
      )
    }

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.headertext}>Detail Pesanan</Text>
        </View>
          <ScrollView contentContainerStyle={{flex:1}}>
            <View>
              <Text style={{fontSize:15,fontWeight:'bold',color:'#00000095',marginTop:20,marginLeft:20}} >No Tagihan : {id}</Text>
            </View>
            <View style={{width:Dimensions.get('window').width,height:180,elevation:1,backgroundColor:'#fff'}}>
              <Text style={{fontSize:18,fontWeight:'bold',margin:20}} >Informasi Tagihan</Text>
              
                <View style={{justifyContent:'center',width:Dimensions.get('window').width,height:60,elevation:5,backgroundColor:'#fff'}}>
                  <View style={{marginLeft:20,marginRight:20}}>
                      <Text style={{width:200,color:'#000000',fontWeight:'bold',fontSize:15}}>Status Tagihan</Text>
                      {renderIf(data.status === 'menunggu' ? true : false)(
                      <Text style={{color:'#00000090',fontSize:14,}}>menunggu</Text>
                      )}
                      {renderIf(data.status === 'selesai' ? true : false)(
                      <Text style={{color:'#00000090',fontSize:14,}}>pembayaran berhasil</Text>
                      )}
                      {renderIf(data.status === 'batal' ? true : false)(
                      <Text style={{color:'#00000090',fontSize:14,}}>pembayaran gagal</Text>
                      )}
                  </View>
                </View>

                <View style={{justifyContent:'center',width:Dimensions.get('window').width,height:60,elevation:5,backgroundColor:'#fff',borderWidth:0.5}}>
                  <View style={{marginLeft:20,marginRight:20}}>
                    <Text style={{width:200,color:'#000000',fontWeight:'bold',fontSize:15}}>Total Tagihan</Text>
                    <Text style={{color:'#00000090',fontSize:14,}}>Rp {numeral(data.total).format('0,0')}</Text>
                  </View>
                </View>

                <TouchableOpacity onPress={()=> this.setState({loading:true})} style={{justifyContent:'center',width:Dimensions.get('window').width,height:60,elevation:5,backgroundColor:'#fff'}}>
                  <View style={{marginLeft:20,marginRight:20}}>
                    <Text style={{width:200,color:'#000000',fontWeight:'bold',fontSize:15}}>Metode Pembayaran</Text>
                    <Text style={{color:'#00000090',fontSize:14,}}>{data.metode}</Text>
                  </View>
                </TouchableOpacity>

              <Text style={{fontSize:18,fontWeight:'bold',margin:20}} >Barang Pembelian</Text>
              {items.map((data,index)=>
                <TouchableOpacity style={{flexDirection:'row',marginTop:20,width:Dimensions.get('window').width}} >
                  <Image style={{flex:1,width:85,height:85,marginHorizontal:25}} source={{uri:data.data.image}}/>
                    <View style={{flex:2}}>
                      <Text style={{width:140,color:'#000000',fontWeight:'bold',fontSize:18,marginTop:5}}>{data.data.nama}</Text>
                      <Text style={{color:'#000000',fontWeight:'bold',fontSize:14,color:'#ed5107'}}>Rp. {numeral(data.data.harga).format('0,0')}</Text>
                      <Text style={{color:'#000000',fontWeight:'bold',fontSize:15,marginTop:5}}>Item : {data.data.jumlah} {data.data.satuan}</Text>
                    </View>
                    <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                  </View>
                </TouchableOpacity>
              )}
            </View>
          </ScrollView>
        <Modal animationType="fade" transparent={true} visible={this.state.loading}>
          <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <View style={{backgroundColor:'#fff',width:300,height:300,elevation:10,borderRadius:10}}>
              <TouchableOpacity onPress={()=> this.setState({loading:true})} style={{marginLeft:250,marginTop:5,alignItems:'center',justifyContent:'center',borderRadius:100}}>
                <Text style={{textAlign:'center'}}>tutup</Text>
              </TouchableOpacity>
              <Text style={{marginBottom:20,marginLeft:8,fontSize:18,fontWeight:'bold',color:'#000000'}} >{this.state.nama}</Text>
              
              <View style={{flex:1,alignItems:'center'}}>
                <Image source={{uri:this.state.image}} style={{flex:1,width:100,height:100}}/>
                <View style={{flex:1,justifyContent:'center',flexDirection:'row',marginTop:10}}>
                  <TouchableWithoutFeedback disabled={this.state.text !== 1 ? false : true} onPress={ () => this._decrementCount() }>
                   <Image style={{width:25,height:25}} source={require('../assets/minus.png')}/>
                  </TouchableWithoutFeedback>
                   <Text style={{paddingLeft:20,paddingRight:20,color:'#000000',fontSize:18}}> {this.state.text} </Text>
                  <TouchableWithoutFeedback onPress={ () => this._incrementCount() }>
                   <Image style={{width:25,height:25}} source={require('../assets/plus.png')}/>
                  </TouchableWithoutFeedback>
                </View>
                <View style={{flexDirection:'row',flex:1,marginLeft:10}}>
                  <View style={{flex:1}}>
                    <Text style={{fontSize:17,fontWeight:'bold',color:'#000000'}} >SubTotal</Text>
                    <Text style={{fontSize:15,fontWeight:'bold',color:'#ed5107'}} >{numeral(this.state.harga).format('0,0')}</Text>
                  </View>
                  <View style={{flex:1,alignItems:'flex-end',marginRight:10}}>
                    <TouchableOpacity style={{borderRadius:10,width:100,height:40,backgroundColor:'#293462',alignItems:'center',justifyContent:'center'}} onPress={this.cart}>
                      <Text style={{fontSize:15,color:'#fff',fontWeight:'bold'}}>Order</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>

            </View>
          </View>
        </Modal>
      </View>
    );
  }

  absen =() => {
    this.props.navigation.navigate('pembelian')
  }

  delete(data){
    const db = firebase.firestore()
    Alert.alert(
      'Opss',
      'Ingin menghapus data ini ?',
      [
        {
          text: 'tidak',
          onPress: () => console.log('Cancel Pressed'),
          style: 'tidak',
        },
        {text: 'OK', onPress: () => 
          store.get('uid').then((uid)=>{
            db.collection("user").doc(uid).collection('perizinan').doc(data.id).delete()
          })
        },
      ],
      {cancelable: false},
    );
  }

}
