'use strict';

import React, {
  Component,
} from 'react';
import {
  Linking,
  Platform,
  ActionSheetIOS,
  Dimensions,
  View,
  Text,
  Navigator,
  Image,
  StyleSheet,
  TouchableOpacity,
  BackHandler
} from 'react-native';
import firebase from 'react-native-firebase';
import { GiftedChat,Bubble } from 'react-native-gifted-chat'
import store from 'react-native-simple-store';
// import RNFetchBlob from 'react-native-fetch-blob'
// import ImagePicker from 'react-native-image-picker'

export default class room extends Component{
	constructor(props) {
	  super(props);
	  this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
	  this.state = {
	  	id:'',
	  	name:'',
        loading: false,
	  };
	  this.renderFooter = this.renderFooter.bind(this);
	}

	uid=''
	messagesRef = null

	state = {
		messages:[]
	}

	loadMessages(callback){
		const { params } = this.props.navigation.state;
		this.messagesRef = firebase.database().ref('groub').child(params.chat).child('message')
		this.messagesRef.off()
		const onReceive = (data)=>{
			const message = data.val()
			callback({
				_id:data.key,
				text:message.text,
				createdAt:new Date(message.createdAt),
				image: message.image,
				user:{
					_id:message.user._id,
					name:message.user.name,
				},
			})
		}
		this.messagesRef.limitToLast(20).on('child_added',onReceive);
	}

	sendMessage(messages){
		messages.forEach((message)=>{
			this.messagesRef.push({
				text:message.text,
				createdAt:firebase.database.ServerValue.TIMESTAMP,
				user:message.user,
			})
		})
	}

	closeChat(){
		if(this.messagesRef){
			this.messagesRef.off()
		}
	}

	componentDidMount(){
		this.loadMessages((message)=>{
			this.setState((previousState)=>{
				return{
					messages:GiftedChat.append(previousState.messages,message),
				}
			})
		})

		store.get('uid').then((res)=>{
			this.setState({ id: res})
		})

		store.get('nama').then((res)=>{
			this.setState({ name: res})
		})
	}

	  componentWillMount() {
	  BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
	  }

	  componentWillUnmount() {
	  BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
	  }

	  handleBackButtonClick() {
	  	this.props.navigation.navigate('Chat')
	  return true;
	  }

	renderFooter(props) {
	const { navigate } = this.props.navigation;
	const { params } = this.props.navigation.state;
      return (
        <View>
          <TouchableOpacity onPress={() => navigate('uploadimage',{id:this.state.id,name:this.state.name,chat:params.chat})}>
          	<Image source={require('../assets/chat.png') }style={{width:30,height:30,marginLeft:5,marginBottom:5,marginTop:5}}/>
          </TouchableOpacity>
        </View>
      )
    }

	render(){
		return(
		<GiftedChat
			messages={this.state.messages}
			onSend={(message)=>{
				this.sendMessage(message);
			}}
			user={{
				_id: this.state.id,
				name: this.state.name,
			}}
			// renderBubble={this.renderBubble}
			renderActions={this.renderFooter}
			/>
		)
	}
}

const styles = StyleSheet.create({
  footerContainer: {
    marginTop: 5,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 10,
  },
  footerText: {
    fontSize: 14,
    color: '#aaa',
  },
});