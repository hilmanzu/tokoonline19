import React, {Component} from 'react';
import {TouchableHighlight,ActivityIndicator,Alert,Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import styles from '../style/tagihan'
import { createMaterialTopTabNavigator,createSwitchNavigator, createStackNavigator, createAppContainer,createBottomTabNavigator } from 'react-navigation';
import store from 'react-native-simple-store';
import moment from 'moment'
import 'moment/locale/id'
moment.locale('id')
import firebase from "react-native-firebase"
import renderIf from '../assets/renderIf'
import sub_tagihan from './sub_tagihan'

var numeral = require('numeral');

class tagihan extends Component{

  constructor(props){
    super(props)
    this.delete = this.delete.bind(this);
    this.state = {
      dashboard:[],
      loading:true,
      touch:''
      }
    }

  componentDidMount(){
    store.get('uid')
    .then((res)=>{
      const db = firebase.firestore();
      const base = db.collection("transaksi").orderBy('waktu','desc')
      base.onSnapshot((querySnapshot)=>{
          var data = []
            querySnapshot.forEach((doc)=>{
              let item = data
              item.push({
                data : doc.data(),
                id   : doc.id
              })
            })
          this.setState({
            dashboard : data.filter((data)=> data.data.id_user === res),loading:false
          })
        })
    })

  }

  render() {
  const { dashboard} = this.state

    return (
      <View style={styles.container}>
          <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{flexGrow : 1, alignItems:'center',marginTop:20}}>
            {dashboard.map((data)=>
                <TouchableOpacity onPress={this.absen} style={{flexDirection:'row',marginTop:5,width:320,borderWidth:1,borderRadius:10,height:100,margin:5,justifyContent:'center',alignItems:'center'}} >
                  <Image style={{width:70,height:70,marginHorizontal:25}} source={require('../assets/buy.png')}/>
                  <View>
                    <Text style={{width:220,color:'#000000',fontWeight:'bold',fontSize:18}}>Pembelian Barang</Text>
                    {renderIf(data.data.status === 'menunggu' ? true : false)(
                    <Text style={{color:'#00000090',fontSize:14,}}>menunggu pembayaran</Text>
                    )}
                    {renderIf(data.data.status === 'selesai' ? true : false)(
                    <Text style={{color:'#00000090',fontSize:14,}}>pembayaran berhasil</Text>
                    )}
                    {renderIf(data.data.status === 'batal' ? true : false)(
                    <Text style={{color:'#00000090',fontSize:14,}}>pembayaran gagal</Text>
                    )}
                    <Text style={{color:'#000000',fontWeight:'bold',fontSize:15,marginTop:5}}>Total Rp. <Text style={{color:'#ed5107'}}>{numeral(data.data.total).format('0,0')}</Text> </Text>
                  </View>
                </TouchableOpacity>
            )}
          </ScrollView>
      </View>
    );
  }

  absen =() => {
    this.props.navigation.navigate('sub_tagihan')
  }

  delete(data){
    const db = firebase.firestore()
    Alert.alert(
      'Opss',
      'Ingin menghapus data ini ?',
      [
        {
          text: 'tidak',
          onPress: () => console.log('Cancel Pressed'),
          style: 'tidak',
        },
        {text: 'OK', onPress: () => 
          store.get('uid').then((uid)=>{
            db.collection("user").doc(uid).collection('perizinan').doc(data.id).delete()
          })
        },
      ],
      {cancelable: false},
    );
  }

}

const Base = createStackNavigator({
  Home       : tagihan,
  sub_tagihan: sub_tagihan,
},{headerMode   : 'none'})

export default createAppContainer(createSwitchNavigator(
  {
    Base       : Base,
  },
  {
    initialRouteName: 'Base',
  }
));


