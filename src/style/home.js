import {Dimensions,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "rgb(255,255,255)",
    flexDirection: "row"
  },
  header:{
    width:Dimensions.get('window').width,
    height:50,
    backgroundColor:'#293462',
    alignItems:'center',flexDirection:'row'
  },
  headertext:{
    color:'#fff',
    fontWeight:'bold',
    marginLeft:20
  },
  tabicon:{
    width:20,
    height:20
  },
  scroll:{
    alignItems:'center'
  }

});

export default styles