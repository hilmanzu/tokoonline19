import {Dimensions,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: "rgb(255,255,255)",
    alignItems:'center'
  },
  header:{
    width:Dimensions.get('window').width,
    height:50,
    backgroundColor:'#293462',
    alignItems:'center',flexDirection:'row'
  },
  headertext:{
    color:'#fff',
    fontWeight:'bold',
    marginLeft:20
  },
  tabicon:{
    width:20,
    height:20
  },
  scroll:{
    alignItems:'center'
  },
  scrollitems:{
    flexGrow:1
  },
    titlei:{
    position:'absolute',
    marginLeft:20,
    marginTop:20,
    color:'#000000',
    fontSize:18,
    fontWeight:'bold',
  },

  image:{
      width:30,
      height:30,
      tintColor:'#000000'
    },

  hargai:{
    position:'absolute',
    marginLeft:20,
    marginRight:20,
    marginTop:50,
    color:'#000000',
    fontSize:12,
  },

  stok:{
    position:'absolute',
    marginLeft:20,
    marginRight:20,
    marginTop:80,
    color:'#000000',
    fontSize:15,
  },

  stokmodal:{
    marginTop:10,
    color:'#000000',
    fontSize:18,
    marginBottom:10,
    textAlign:'center'
  },

  imghi:{
    width:150,
    height:120,
    margin:10,
    borderRadius:10
  },

  imgbi:{
    backgroundColor: `rgba(48, 66, 77, 0.55)`,
    width:150,
    height:120,
    borderRadius:10,
    margin:10,
    position: `absolute`
  },

  imghim:{
    width:150,
    height:120,
    borderRadius:10
  },

  imgbim:{
    backgroundColor: `rgba(48, 66, 77, 0.55)`,
    width:150,
    height:120,
    borderRadius:10,
    position: `absolute`
  },
  daftar: {
    marginTop:10,
    width: 120,
    height: 50,
    backgroundColor: "rgba(74,144,226,1)",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    elevation: 5,
    borderRadius: 10,
    borderColor: "rgba(74,144,226,1)",
    borderWidth: 1,
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowColor: "rgba(0,0,0,1)",
    shadowRadius: 0
  },
  textdaftar: {
    color: "rgba(255,255,255,1)"
  },
});

export default styles