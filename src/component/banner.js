import React, {Component} from 'react';
import {StyleSheet,View,ListView,Image,TouchableOpacity,Dimensions,Text} from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import firebase from "react-native-firebase"

const { width: viewport } = Dimensions.get('window')
const sliderWidth1 = wp(100)
const itemHorizontalMargin1 = wp(0.30)
const sliderWidth = viewport
const itemWidth = (sliderWidth1 + (itemHorizontalMargin1 * 2)) - 10

function wp(percentage){
  const value = (percentage * viewport) / 100
  return Math.round(value)
}


class banner extends Component{

  constructor(props){
    super(props);
    this.state = {
      data : [],
    }  
  }

  componentDidMount(){
  const db = firebase.firestore();
  const docRef = db.collection("banner")
    docRef.onSnapshot(async(querySnapshot)=>{
      var data = []
      querySnapshot.forEach((doc)=>{
        let datas = data
        datas.push({
          id   : doc.id,
          data : doc.data()
        })
      })
      this.setState({
        data : data
      })
    })
  }

   _renderItem ({item, index}) {
        return (
            <View style={{flex:1,alignItems:'center',justfyContent:'center'}}>
                <Image source={{uri:item.data.image}} style={{width:320 ,height:180,marginVertical:10,borderRadius:10}}/>
            </View>
        );
    }

    get pagination () {
        const { data, activeSlide } = this.state;
        return (
            <Pagination
              dotsLength={data.length}
              activeDotIndex={activeSlide}
              containerStyle={{borderRadius: 5,width:20,height:5,position:'absolute',marginTop:150,marginLeft:300}}
              dotStyle={{width: 10,height: 10,borderRadius: 5,backgroundColor: '#B57BA6'}}
              inactiveDotStyle={{backgroundColor: 'rgba(0, 0, 0, 0.75)'}}
              inactiveDotOpacity={0.4}
              inactiveDotScale={0.6}
            />
        );
    }

  render() {
    const BannerWidth = Dimensions.get('window').width;
    const BannerHeight = 140;
    const {data} = this.state
    console.log(data)
    
    return (
      <View style={{flex:1}}>
        <Carousel
          ref={(c) => { this._carousel = c; }}
          data={data}
          autoplay = {true}
          loop = {true}
          renderItem={this._renderItem}
          onSnapToItem={(index) => this.setState({ activeSlide: index }) }
          sliderWidth={Dimensions.get('window').width}
          itemWidth={Dimensions.get('window').width}
          sliderHeight={180}
          itemHeight={180}
        />
        {this.pagination}
      </View>
    );
  }
}

export default banner