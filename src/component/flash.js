import React, {Component} from 'react';
import {Modal,BackHandler,Alert,Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import store from 'react-native-simple-store';
import firebase from "react-native-firebase"

export default class flash extends Component{

  constructor(props) {
    super(props)
      this.state = {
        flash: [],
      }
  }

  componentDidMount(){
      const db = firebase.firestore();
      const news = db.collection("flash")
      news.onSnapshot(async(querySnapshot)=>{
          var data = []
            querySnapshot.forEach((doc)=>{
              let item = data
              item.push({
                data   : doc.data(),
                id     : doc.id,
                number : data.length + 1
              })
            })
          this.setState({
            flash : data
          })
      })
  }

  render() {
    const {flash} = this.state
    return (
      <View style={styles.container}>
          <View style={{width:360,marginTop:20}}>
            <View style={{flexDirection:'row'}}>
              <Text style={{marginLeft:20,marginBottom:10}}>
                Flash Deal
              </Text>
            </View>
              <ScrollView scrollEventThrottle={5} horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={{flexGrow : 1,height:120,paddingBottom:20,paddingLeft:5,paddingRight:5}}>
                {flash.map((data)=>
                  <TouchableOpacity onPress={()=> Alert.alert('Opss','Fitur ini Segera Hadir')} style={{borderRadius:10,width:200,height:120,backgroundColor:'#000000',marginLeft:10}}>
                    <ImageBackground imageStyle={{opacity: .6,borderRadius:10}} source={{uri : data.data.image}} style={{width:200,height:120}}>
                      <Text numberOfLines={3} style={{width:180,position:'absolute',color:'#fff',fontWeight:'bold',fontSize:15,paddingTop:60,marginLeft:10}}>{data.data.nama}</Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}
              </ScrollView>
          </View>
          <View style={{width:360,marginTop:20,marginBottom:60}}>
            <View style={{flexDirection:'row'}}>
              <Text style={{marginLeft:20,marginBottom:10}}>
                Paling Laris
              </Text>
            </View>
              <ScrollView scrollEventThrottle={5} horizontal={true} showsHorizontalScrollIndicator={false} contentContainerStyle={{flexGrow : 1,height:120,paddingBottom:20,paddingLeft:5,paddingRight:5}}>
                {flash.map((data)=>
                  <TouchableOpacity onPress={()=> Alert.alert('Opss','Fitur ini Segera Hadir')} style={{borderRadius:10,width:200,height:120,backgroundColor:'#000000',marginLeft:10}}>
                    <ImageBackground imageStyle={{opacity: .6,borderRadius:10}} source={{uri : data.data.image}} style={{width:200,height:120}}>
                      <Text numberOfLines={3} style={{width:180,position:'absolute',color:'#fff',fontWeight:'bold',fontSize:15,paddingTop:60,marginLeft:10}}>{data.data.nama}</Text>
                    </ImageBackground>
                  </TouchableOpacity>
                )}
              </ScrollView>
          </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    alignItems:'center',
    backgroundColor:'#fff',
  },
  scrolview:{
    alignItems:'center'
  },
  path1: {
    width: Dimensions.get('window').width,
    height: 150,
    borderColor: '#420000',
    borderStyle: 'solid',
    borderWidth: 1,
    backgroundColor: '#420000',
    borderBottomEndRadius:10,
    borderBottomStartRadius:10,
    alignItems:'center',
  },
  path2: {
    flexDirection:'row',
    justifyContent:'center',
    marginTop:10,
    width: Dimensions.get('window').width
  },
  image: {
    width: 50,
    height: 50,
    borderRadius:100,
  },
  imageBack: {
    width: 60,
    height: 60,
    position:'absolute',
    borderRadius:90,
    marginTop:90,
    backgroundColor:'#fff',
    alignItems:'center',
    justifyContent:'center',
  },
  button:{
    alignItems:'center',
  },
  imageIcon: {
    width: 25,
    height: 25,
  },
  imageBackIcon: {
    width: 50,
    height: 50,
    borderRadius:90,
    marginTop:5,
    backgroundColor:'#fff',
    alignItems:'center',
    justifyContent:'center',
    elevation:10
  },
  textImageIcon: {
    marginTop:10,
    width: 80,
    color:'#00000090',
    textAlign:'center',
    fontSize:12
  },
  welcome: {
    width: 250,
    color: '#ffffff',
    fontFamily: 'Segoe UI',
    fontSize: 19,
    fontWeight: '700',
    textAlign:'center',
    marginTop:20
  },


})