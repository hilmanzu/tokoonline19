import React, {Component} from 'react';
import {Modal,BackHandler,Alert,Dimensions,ScrollView,Image,ImageBackground,Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import store from 'react-native-simple-store';
import firebase from "react-native-firebase"

export default class menu extends Component{

  render() {

    return (
      <View style={styles.container}>
          <View style={styles.path2}>
            <TouchableOpacity style={styles.button} onPress={(()=>this.props.navigation.navigate('checkin'))}>
              <View style={styles.imageBackIcon}>
                <Image style={styles.imageIcon} source={require('../assets/listrik.png')}/>
              </View>
              <Text style={styles.textImageIcon}>Listrik{"\n"}Prabayar</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={(()=>this.props.navigation.navigate('reimburse'))}>
              <View style={styles.imageBackIcon}>
                <Image style={styles.imageIcon} source={require('../assets/pulsa.png')}/>
              </View>
              <Text style={styles.textImageIcon}>Pulsa{"\n"}Prabayar</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.button} onPress={(()=>this.props.navigation.navigate('perizinan'))}>
              <View style={styles.imageBackIcon}>
                <Image style={styles.imageIcon} source={require('../assets/game.png')}/>
              </View>
              <Text style={styles.textImageIcon}>Voucher{"\n"} Game</Text>
            </TouchableOpacity>
          </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    alignItems:'center',
    backgroundColor:'#fff',
  },
  scrolview:{
    alignItems:'center'
  },
  path1: {
    width: Dimensions.get('window').width,
    height: 150,
    borderColor: '#420000',
    borderStyle: 'solid',
    borderWidth: 1,
    backgroundColor: '#420000',
    borderBottomEndRadius:10,
    borderBottomStartRadius:10,
    alignItems:'center',
  },
  path2: {
    flexDirection:'row',
    justifyContent:'center',
    marginTop:10,
    width: Dimensions.get('window').width
  },
  image: {
    width: 50,
    height: 50,
    borderRadius:100,
  },
  imageBack: {
    width: 60,
    height: 60,
    position:'absolute',
    borderRadius:90,
    marginTop:90,
    backgroundColor:'#fff',
    alignItems:'center',
    justifyContent:'center',
  },
  button:{
    alignItems:'center',
  },
  imageIcon: {
    width: 25,
    height: 25,
  },
  imageBackIcon: {
    width: 50,
    height: 50,
    borderRadius:90,
    marginTop:5,
    backgroundColor:'#fff',
    alignItems:'center',
    justifyContent:'center',
    elevation:10
  },
  textImageIcon: {
    marginTop:10,
    width: 80,
    color:'#00000090',
    textAlign:'center',
    fontSize:12
  },
  welcome: {
    width: 250,
    color: '#ffffff',
    fontFamily: 'Segoe UI',
    fontSize: 19,
    fontWeight: '700',
    textAlign:'center',
    marginTop:20
  },


})