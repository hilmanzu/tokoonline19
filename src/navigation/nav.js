import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { createMaterialTopTabNavigator,createSwitchNavigator, createStackNavigator, createAppContainer,createBottomTabNavigator } from 'react-navigation';
///opening
import Splashscreen from '../screen/splashscreen'

///auth
import login from '../screen/login'
import register from '../screen/register'
//menu
import menu from '../screen/home'
// import trans from '../screen/transaksi'
///chat
import chat from '../screen/chat'
import chat_room from '../screen/chat_room'
import profile from '../screen/profile'
///perizinan
import sub_home from '../screen/sub_home'
// ///chat
import order from '../screen/order'
import trans from '../screen/tagihan'
import sub_transaksi from '../screen/sub_tagihan'
import pembayaran from '../screen/pembayaran'

const Tab = createBottomTabNavigator({
  menu          : menu,
  tagihan       : trans,
  Chat          : chat,
  Profile       : profile
},{
  navigationOptions:{ header:{ visible:true }},
    lazy:false,
    tabBarPosition: 'bottom',
    animationEnabled: true,
    swipeEnabled: true,
    tabBarTextFontFamily: 'quicksand',
    tabBarOptions: {
      activeTintColor: '#420000',
      inactiveTintColor: '#00000070',
      showIcon: true,
      showLabel: true,
      style:{height: 50,fontWeight:'bold'}
    },
})

// const Absensi = createStackNavigator({
//   subabsen      : subabsen,
// },{headerMode   : 'none'});

// const webview = createStackNavigator({
//   news      : news
// },{headerMode   : 'none'});

const Auth = createStackNavigator({
  login         : login,
  register      : register
},{headerMode   : 'none'})

const Opening = createStackNavigator({
  Splash  : Splashscreen
},{headerMode   : 'none'})

const Base = createStackNavigator({
  Tab            : Tab,
  chat_room      : chat_room,
  sub_home       : sub_home,
  sub_transaksi  : sub_transaksi,
  order          : order,
  pembayaran     : pembayaran
},{headerMode   : 'none'})



export default createAppContainer(createSwitchNavigator(
  {
    Opening   : Opening,
    Auth 		  : Auth,
    Base 		  : Base 
  },
  {
    initialRouteName: 'Opening',
  }
))

